"""MLPCA module used to perform Principal Component Analysis on a set of training images.

This module is used to:
    1) Perform PCA on Training Images.
    2) Store column stacked images as an array.
    3) Store Projected Components array of the training images.
    4) Compute and store the mean of projected components vectors array of the training images.
    
All methods in this class are static, so they can be used directly without creating an object.
"""


import glob as g                                 #Used for getting images in a directory.
import numpy as np                               #Numpy arrays.
from sklearn.decomposition import RandomizedPCA  #RandomizedPCA 
from Utilities.Constants import Constants        #For Constants.
import logging                                   #For Logging.
from PIL import Image                            #Opening and manipulating Images.
from sklearn.externals import joblib             #For saving PCA objects.
from random import shuffle                       #For shuffling lists.
import math                                      #For mathematical functions.



#Get logger
logger = logging.getLogger('MainLogger')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()

class MLPCA:
    """
    MLPCA Class encapsulates all functionalities to perform offline machine learning using PCA on 
    set of training images.
    """
    
    @staticmethod
    def performPCA(outputFolderPath, columnStackedImagesAvailable = False, whiten = False,numberOfImagesToRead=-1, convertToGrayScale=True, nComponents = 45):
        """
        Performs PCA on set of images that has been processed by the TrainingDataManager.
        
        Args:
            - outputFolderPath (String): Full path to the directory that will hold the outputs.
            - columnStackedImagesAvailable (boolean): Indicates if the images are avaialable as column 
            stacked array, false by default.
            - whiten (boolean): Indicates if the randomizedPCA should whiten the samples, false by 
            default.
            - numberOfImagesToRead (int): Number of images to read in the directory, if the number is
            not given, the method will read all images.
            - convertToGrayScale (Boolean): If true, the read images will be converted to grayscale,if 
            not given the default value is True.
            - nComponents (int): Number of principal components, n = 45 by default.
        Returns:
            - ColumnStackedImages (Numpy array): array resulting from column stacking of the images.
            - ProjectedComponentVectors (Numpy array): array of the projected component vectors
            resulting from PCA.
            - PCAObject (Pickle file): Scikit-learn RandomizdPCA object serilization.
            - MeansArrays (Dictionary): dictionary of means of each category of the images.
        """
        
        #Get the column Stacked array.
        #IF the array is available, load it.
        if columnStackedImagesAvailable:
            columnStackedArray = np.load(Constants.DATA_PATH + Constants.PCA_CSINAME + ".npy")
        #If not, compute it. it.
        else:        
            #Get the images Array.
            imagesArray = np.load("./TrainingData/"+ Constants.TRNG_IMGS)
            columnStackedArray = PCAHelper.performColumnStacking(imagesArray)
        
        
        #Perform PCA on columnStackedArray.
        #Create the pca object.
        pca = RandomizedPCA(n_components=nComponents, whiten = whiten)
        
        #Transpose the columnStackedArray.
        transposedArray = np.transpose(columnStackedArray)
        
        #Perform PCA on the transposedArray, get the projected Components Vectors.
        projectedComponentsVectors = pca.fit_transform(transposedArray)
        
        #Get the results of the images, to be used in the means arrays.
        results = np.load("./TrainingData/" + Constants.TRNG_RSLTS)
        
        #Get the means arrays.
        meansArrays = PCAHelper.getMeansArrays(projectedComponentsVectors, results)
        
        #Save all objects.
        #Save the projected components vectors.
        np.save(outputFolderPath + Constants.PCA_PCVNAME, projectedComponentsVectors)
        
        #Save the column stacked array.
        np.save(outputFolderPath + Constants.PCA_CSINAME, columnStackedArray)
        
        #Save the PCA object.
        joblib.dump(pca, outputFolderPath + Constants.PCA_OBJNAME)
        
        #Save the meansArrays.
        joblib.dump(meansArrays, outputFolderPath + Constants.PCA_MNSNAME)
        
        
class PCAHelper:
    """
    PCAHelper class encapsulates all helper methods useful for performing PCA. 
    """
    
    
    @staticmethod
    def getMeansArrays(projectedComponentsVectors, results):
        """
        Compute the mean of each category of results (such as: d12, g23, .. so on), and return it as a data structure. The returned
        Data structure is as follows:
        Dictionary encapsulating each category with its mean as follows: 
        dict[key] = value, where key = category name, value = category mean.
        
        Args:
            - projectedComponentsVectors (numpy array): Array of PCVs.
            - results (array): Array of training images results.
        Returns:
            - Means (Dictionary): dictionary described in the method's description.
        """
        #Create the means dictionary
        means = dict()


        #Loop through all projected components vectors. 
        for i, vector in enumerate(projectedComponentsVectors):
            #Get the result of the vector.
            vectorResult = results[i]

            #If the result has not been seen yet.
            if vectorResult not in means.keys():
                #Create a new mean array for that result.
                means[vectorResult] = np.array(vector)
            #Else if the result has been seen.
            else:
                #Append the vector to the numpy array.
                means[vectorResult] = np.column_stack([means[vectorResult], vector])
           
        #Loop through all categorized vectors and compute their means after transposing them.
        for category in means.keys():
            #Transpose it.
            means[category] = means[category].transpose()
            #Calculate the mean using the columns.
            means[category] = means[category].mean(axis = 0)
        
        
        #Return the means dictioanary.
        return means
 

        
    
    
    @staticmethod
    def performColumnStacking(images):
        """
        Performs columnStacking on set of images.
        
        Args:
            - images (numpy array): Array of images numpy arrays 
        Returns:
            - ColumnStackedImages (Numpy array): array resulting from column stacking of the images.
        """
        
        #Get the number of images arrays.
        numberOfArrays = len(images)
        
        #Check if the number of arrays < 0 -> handle error.
        if numberOfArrays < 1:
            return None
        #If there's only one image, column stack it and return it.
        elif numberOfArrays == 1:
            return np.column_stack(images[0].flatten('F'))
        
        #If there's more than one image.
        else:
            #Create a resulting array, by stacking the first two images.
            resultArray = np.column_stack((images[0].flatten('F'), images[1].flatten('F')))
            
            #Loop through the rest of images, if there's any.
            for i in range(2,numberOfArrays):
                #Column stack the result array with the new image.
                resultArray = np.column_stack((resultArray, images[i].flatten('F')))
                
                #For printing.
                if i%5 == 0:
                    logger.info(Constants.PRINT_ERASE)
                    logger.info("Done Stacking for: {0:.2f}%\n".format(float(i)/numberOfArrays*100))
            
            #Print the result.
            logger.info(Constants.PRINT_ERASE)
            logger.info("Done Stacking: 100%\n")
            
            return resultArray
        

        
        
        
        
        
        
        
        
    
    