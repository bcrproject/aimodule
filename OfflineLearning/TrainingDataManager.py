"""TrainingDataManager used to prepare and manage training data from different sources. 
For each source, this class must be subclassed to implement the interface methods.  

Some of the methods are implemented in the super class, since they are common funcionalties for all.
"""
from abc import ABCMeta, abstractmethod          #For abstract.
import numpy as np                               #Numpy arrays.
from Utilities.Constants import Constants        #For Constants.
import logging                                   #For Logging.
import glob as g                                 #Used for getting images in a directory.
from random import shuffle                       #For shuffling lists.
from PIL import Image                            #Opening and manipulating Images.
import math                                      #For mathematical functions.


#Get logger
logger = logging.getLogger('MainLogger')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
logger.addHandler(ch)

class TrainingDataManager:
    __metaclass__ = ABCMeta
    
    
    def getImagesInList(self, images,  numberOfImagesToRead = -1, convertToGrayScale = True):
        """
        Concrete method to read images with full paths specified in the given list. and return them as numpy array of images.
        
        Args:
            - images (list): list of images full paths, all images must be PNG. 
            - numberOfImagesToRead (int): Number of images to read in the directory, if the number is
            not given, the method will read all images in the directory.
            - convertToGrayScale (Boolean): If true, the read images will be converted to grayscale,if 
            not given the default value is True.
        Returns:
            - readImages (list of numpy arrays): list of images, each image is numpy array.
        """
        #Determine the imagesToRead, if the numberOfImagesToRead given is not -1 and is less than or 
        #equal to the number of files found in the given path. Otherwise, number of imagesToRead will 
        #be equal to the number of images found in the given path. 
        imagesToRead = numberOfImagesToRead if numberOfImagesToRead <= len(images) and numberOfImagesToRead != -1 else len(images)
    
        #Logging.
        logger.info("Number of images that will be read: " + str(imagesToRead) + "\n\n")
    
        #Create an array of images' numpy arrays.
        imagesArray = []
        
        #Loop through all images that need to be read.
        for i in range(0, imagesToRead):
            #Open the image.
            image = Image.open(images[i])
            
            #Check if the image needs to be converted to grayscale.
            if convertToGrayScale:
                image = image.convert('L')
                
            #Logging
            if i % 5 == 0:
                logger.info(Constants.PRINT_ERASE)
                logger.info("Done Reading for: {0:.2f}%\n".format(float(i)/imagesToRead*100))
                
                
            #Append the image to the images array, after converting it to numpy array.
            imagesArray.append(np.array(image.getdata()))
            
        #Printing.
        logger.info(Constants.PRINT_ERASE)
        logger.info("Done Reading for: 100%\n".format(float(i)/imagesToRead*100))
         
        #Return the imagesArray
        return imagesArray
    
        
    @abstractmethod
    def getImagesNamesInPath(self, imagesPath):       
        """
        Abstract method to read images names and return them as list of full paths.
        
        Args:
            - imagesPath (String): Full path to the directory containing the images. All Images must
            be of type PNG.
        Returns:
            - readImages (list): list of images full paths and names.
        """
        pass
    
    @abstractmethod
    def saveTrainingAndTestingImagesWithResults(self, imagesPath, trainingImagesPercentage,  numberOfImagesToRead = -1, convertToGrayScale = True):
        """
        Abstract method to save two sets of images: Training and testing, along with thier 
        corresponding results, in the path defined in the constants file. 
        Each set of images will be a list of numpy array of images. 
        The results will be two arrays for each set of images, the index of the array corresponds to 
        the index of the image, and the value corresponds to the result of the image.
        Ex: TrainingImagesResults[0] = Result of the first image of the training images. 
        The training and testing images will be split based on the given percentages. 
        
        Args:
            - imagesPath (String): Full path to the directory containing the images. All Images must
            be of type PNG.
            - numberOfImagesToRead (int): Number of images to read in the directory, if the number is
            not given, the method will read all images in the directory.
            - convertToGrayScale (Boolean): If true, the read images will be converted to grayscale,if 
            not given the default value is True.
            - trainingImagesPercentage (int): percentage of the training images set. 
        """
        pass
    
    def splitImagesTwoSets(self, images, results, fstSetPercentage):
        """
        Concrete Splits a list of images to two sets: Training, and testing.
        The images will be split according to the required percentages of each set. 
        Each category of results will have its images split into the required percentages,
        to ensure fair splitting of images of each category. Therefore, the percentage of each set
        the percentage of each category alone: Ex. 70% of list of 2000 images = 70% of each 
        category of images != 0.7*2000, due to rounding.
        
        Args:
            - images (numpy array): Array of images numpy arrays.
            - results (array): Array of training images results, as tuples (image name, result).
            - fstSetPercentage (int): percentage of the first set of images.
        Returns:
            - SplitSets (([FirstSetImages], [SecondSetImages], )): tuple of two lists of images and
            two lists of results. 
        """
        #Create results dictionary.
        imagesCategories = dict()


        #Loop through all images. 
        for i, image in enumerate(images):
            #Get the result of the image.
            imageResult = results[i][1]

            #If the result has not been seen yet.
            if imageResult not in imagesCategories.keys():
                #Create a new list for the image.
                imagesCategories[imageResult] = [image]
                
            #Else if the result has been seen.
            else:
                #Append the image to the list of images in that category.
                imagesCategories[imageResult] += [image]
        
        #Create list for first set.
        firstSet = []
        
        #Create list for second set.
        secondSet = []
        
        #Create a list of results for the first set.
        firstSetResults = []
        
        #Create a list of results for the second set.
        secondSetResults = []
        

        
        #Loop through all categories.
        for category in imagesCategories.keys():
            #Get the list of images in the current category.
            imagesInCategory = imagesCategories[category]
            
            fstSetNumberOfImages = int(math.floor(float(len(imagesInCategory))*fstSetPercentage/100))
            
            
            #Shuffle its contents.
            shuffle(imagesInCategory)
            
            #Loop to assign the first set.
            for i in range(0, fstSetNumberOfImages):
                firstSet.append(imagesInCategory[i])
                firstSetResults.append(category)
            
            #Loop to assign the second set.
            for i in range(fstSetNumberOfImages, len(imagesInCategory)):
                secondSet.append(imagesInCategory[i])
                secondSetResults.append(category)
                
                
        #Return the tuple of first and second sets.
        return (firstSet, secondSet, firstSetResults, secondSetResults)
    