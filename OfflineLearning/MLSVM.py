"""MLSVM module used to perform Support vector machine clasfficiation algorithms on a set of training images, using their projected components values.

All methods in this class are static, so they can be used directly without creating an object.
"""

import numpy as np                               #Numpy arrays.
from sklearn.decomposition import RandomizedPCA  #RandomizedPCA 
from Utilities.Constants import Constants        #For Constants.
import logging                                   #For Logging.
from PIL import Image                            #Opening and manipulating Images.
from sklearn.externals import joblib             #For saving PCA objects.
from sklearn.svm import SVC                      #Support vector classifications.

#Get logger
logger = logging.getLogger('MainLogger')
logger.setLevel(logging.DEBUG)

class MLSVM:
    """
    MLSVM Class encapsulates all functionalities to perform offline machine learning SVM algorithms 
    and save the resulting data structures to be used in online services. 
    """
    
    @staticmethod
    def performSVM(outputFolderPath):
        """
        Performs PCA on set of images that has been processed by the TrainingDataManager.
        
        Args:
            - outputFolderPath (String): Full path to the directory that will hold the outputs.
        """
        
        #Get the PCVs.
        projectedComponents = np.load(Constants.DATA_PATH + Constants.PCA_PCVNAME + ".npy")
        
        #Get the results.
        results = np.load("./TrainingData/" + Constants.TRNG_RSLTS)
        
        #Create the SVC object.
        classifier = SVC()
        
        #Fit the classifier with data.
        classifier.fit(projectedComponents, results)
        
        #Save the PCA object.
        joblib.dump(classifier, outputFolderPath + Constants.SVM_OBJNAME)
        
        
        
        
        