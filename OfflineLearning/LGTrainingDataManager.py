"""LGTrainingDataManager used to prepare and manage training data from the first source (German doctor). 
subclass of TrainingDataManager.

Some of the methods are implemented in the super class, since they are common funcionalties for all.
"""
import numpy as np                               #Numpy arrays.
from Utilities.Constants import Constants        #For Constants.
import logging                                   #For Logging.
import glob as g                                 #Used for getting images in a directory.
from random import shuffle                       #For shuffling lists.
from PIL import Image                            #Opening and manipulating Images.
import math                                      #For mathematical functions.
from TrainingDataManager import TrainingDataManager 

#Get logger
logger = logging.getLogger('MainLogger')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()

class LGTrainingDataManager(TrainingDataManager):
    
    def getImagesNamesInPath(self, imagesPath):       
        """
        method to read images names and return them as list of full paths..
        
        Args:
            - imagesPath (String): Full path to the directory containing the images. All Images must
            be of type PNG.
            - numberOfImagesToRead (int): Number of images to read in the directory, if the number is
            not given, the method will read all images in the directory.
            - convertToGrayScale (Boolean): If true, the read images will be converted to grayscale,if 
            not given the default value is True.
        Returns:
            - readImages (list): list of images full paths and names.
        """
        #Create a files array, containing the full paths of files.
        files = g.glob(imagesPath + "*.png")
        
        #Sort the files from smallest to largest image number.
        #By 1) extracting the file name (Removing the path).
        #   2) Extracting the image number.
        #   3) Sorting the images using the image number.
        files.sort(key=lambda s: int(s[s.rfind('/')+1:].split('_')[0]))
        
        #Return the files.
        return files
        
        
    def saveTrainingAndTestingImagesWithResults(self, imagesPath, trainingImagesPercentage,  numberOfImagesToRead = -1, convertToGrayScale = True):
            """
            Method to save two sets of images: Training and testing, along with thier corresponding 
            results, in the path defined in the constants file. 
            Each set of images will be a list of numpy array of images. 
            The results will be two arrays for each set of images, the index of the array corresponds to 
            the index of the image, and the value corresponds to the result of the image.
            Ex: TrainingImagesResults[0] = Result of the first image of the training images. 
            The training and testing images will be split based on the given percentages. 

            Args:
                - imagesPath (String): Full path to the directory containing the images. All Images must
                be of type PNG.
                - numberOfImagesToRead (int): Number of images to read in the directory, if the number is
                not given, the method will read all images in the directory.
                - convertToGrayScale (Boolean): If true, the read images will be converted to grayscale,if 
                not given the default value is True.
                - trainingImagesPercentage (int): percentage of the training images set. 
            """

            #Get the names of all images in the given path.
            listOfImagesPaths = self.getImagesNamesInPath(imagesPath)

            #Get the images. 
            images = self.getImagesInList(listOfImagesPaths)

            #Get the results of the images.
            results = np.load("./StoredData/" + Constants.EXCEL_RESULTS)

            #Get the training images, testing images, and their results.
            (firstSet, secondSet, firstSetResults, secondSetResults) = self.splitImagesTwoSets(images, results, trainingImagesPercentage)

            
            #log
            logger.info("Saving data...\n")
            
            #Save the results:
            #Save the images.
            np.save("./TrainingData/" + Constants.TRNG_IMGS, firstSet)
            np.save("./TrainingData/" + Constants.TSTNG_IMGS, secondSet)

            #Save the results.
            np.save("./TrainingData/" + Constants.TRNG_RSLTS, firstSetResults)
            np.save("./TrainingData/" + Constants.TSTNG_RSLTS, secondSetResults)
            
            #Log
            logger.info("Done saving data...\n")

    