"""IRMANamingHelper module used to parse the names of classifications and diagnosis from the IRMA databases coding convention numbers.

All methods in this class are static, as they are used as helper functions.
"""

classifications = {
        "d" : [2, "Fat transparent system."],
        "e" : [3, "Fibroid glands system."],
        "f" : [4, "Heterogeneously dense system"],
        "g" : [1, "Extremely dense system"],
        "h" : [0, "Dense system"]
    }
    
diagnosis = {
        "0" : [0, "Unspecified"],
        "1" : [1, "Normal"],
        "2" : [2, "Benign"],
        "3" : [3, "Probably benign"],
        "4" : [4, "Suspiciously abnormal"],
        "5" : [5, "Malignant"]
    }


class IRMANamingHelper:
    """
    Main Class used to encapsulate all functionality provided by this module.
    """

    
    @staticmethod
    def convertIRMAResult(result):
        """
        Converts an IRMA to their meanings according to the enum values in the main server.
        
        Args:
            - result (String): IRMA named result.
        Returns:
            - Classification, Diagnosis (tuple of ints): Results of the image expressed as enum values 
            as specified in the main server detailed diagrams.
            if not found = -1
        """
        classification = -1
        diag = -1
        
        #Extract the classfication.
        if result[0] != None:
            classification = classifications[result[0]][0]        
        
        #Extract the classfication.
        if result[1] != None:
            diag = diagnosis[result[1]][0]
        
        #Return the results
        return (classification, diag)
    
    @staticmethod
    def getResultString(result):
        """
        Converts an IRMA to a meaningful string.
        
        Args:
            - result (String): IRMA named result.
        Returns:
            - Classification, Diagnosis (String): Results of the image expressed in plain english
        """
        
        classification = "Unknown"
        diag = "Unknown"
        
        #Extract the classfication.
        if result[0] != None:
            classification = classifications[result[0]][1]      
        
        #Extract the classfication.
        if result[1] != None:
            diag = diagnosis[result[1]][1]
        
        #Return the results
        return "Classification: %r.\nDiagnosis: %r." %(classification, diag) 
        
        
        
        
        
        
        
        
        
        
        
        