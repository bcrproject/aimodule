"""DicomProcessor provides useful operations on Dicom images.

This module is used for:
    1) Converting Dicom file into PNG.
    2) Extracting a ROI from a Dicom file.
    3) Storing the extracted ROI to PNG.
    4) Creating a thumbnail image from a dicom file.
All methods in this class are static, as they are used as helper methods.
"""

import logging
from PIL import Image
import Converter.med2image as dcmConverter
import dicom
from dicom.tag import Tag 
from Constants import Constants
import os

        
logger = logging.getLogger()
logger.disabled = True

#Hex decimal of dicom patient's information tags to be anonymized.
tagsHex =[0x00100010, 0x00100020,0x00100021,0x00100022,0x00100030,0x00100032,0x00100050,0x00100101,0x00100102,0x00101000,0x00101001,0x00101002,0x00101005,0x00101040,0x00101050,0x00101060,0x00101080,0x00101081,0x00101090,0x00102150,0x00102152,0x00102154,0x00102160,0x00102180,0x001021b0,0x001021c0,0x001021d0,0x001021f0,0x00102202,0x00102203,0x00102293,0x00102294,0x00102295,0x00102296,0x00102297,0x00102298,0x00102299]

class DicomProcessor():
    """
    Main Class used to encapsulate all functions provided by this module.
    """
    
    @staticmethod
    def extractROI(inputDicomPath, outputImageDir, outputImageName, roiWidth, roiHeight, roiX, roiY):
        """
        extractROI static method which extracts a ROI from a DICOM and save it as PNG image.
        
        Args:
            inputDicomPath  (String): path of the input dicom.
            outputImageDir  (String): directory of the output image (without the image).
            outputImageName (String): name of the output image (without the extension).
            roiWidth  (double): width of the ROI.
            roiHeight (double): height of the ROI.
            roiX (double): x coordinate of the center of ROI.
            roiY (double): y coordinate of the center of ROI.
        Result:
            Saves the cropped ROI.
        """
        
        #First convert the dicom to png.
        #Create the DicomConverter object.
        converter = dcmConverter.med2image_dcm(
                                inputFile         = inputDicomPath,
                                outputDir         = outputImageDir,
                                outputFileStem    = outputImageName,
                                outputFileType    = "png")
        
        #Run the converter.
        converter.run()
        
        
        #Open the created png file and convert it to Greyscale.
        image = Image.open(outputImageDir+outputImageName+".png").convert("L")
        
        #Get dimensions of the cropping area.
        leftDimension   = int(roiX)
        upDimension     = int(roiY)
        rightDimension  = int(roiX + roiWidth)
        downDimension   = int(roiY + roiHeight)
        
        #Crop the image.
        croppedImage = image.crop((leftDimension,upDimension, rightDimension, downDimension))
        
        
        try: 
            #Save the image.
            croppedImage.save(outputImageDir+outputImageName+".png", "PNG")
        except Exception as e:
            print "Cannot Save. "
        
        
    
    @staticmethod
    def createThumbnailFromDicom(inputDicomPath, outputImageDir, outputImageName):
        """
        createThumbnailFromDicom, creates a thumbnail image from dicom file and save it.
        
        Args:
            inputDicomPath  (String): path to the dicom file to be converted (including full name).
            outputImageDir (String): path of the output image (Without name of file).
            outputImageName (String): Name of the output image (Without extension)
        Result:
            Saves the thumbnail image (JPEG file).
        """
        #First convert the dicom to Image.
        #Create the DicomConverter object.
        converter = dcmConverter.med2image_dcm(
                                inputFile         = inputDicomPath,
                                outputDir         = outputImageDir,
                                outputFileStem    = outputImageName,
                                outputFileType    = "jpeg")
        
        #Run the converter.
        converter.run()
    
        #Open the created png file.
        image = Image.open(outputImageDir+outputImageName+".jpeg")
        
        #Compress and save.
        image.save(outputImageDir+outputImageName+".jpeg", "JPEG", quality=Constants.COMPRESS_QUALITY)
        
    @staticmethod
    def anonymizeDicom(inputDicomPath):
        """
        Anonymizes all patient's information in the dicom file, by removing them.
        Anonymization is done based on the information provided in: 
        http://www.dicomlibrary.com/terms-of-service
        
        Args:
            inputDicomPath  (String): Absolute full path of the input dicom file, including the dicom extension.
        Result:
            Saves the annonymized dicom over the old dicom.
        """
        
        
        #Read the dicom file.
        dicomFile = dicom.read_file(inputDicomPath)
        
        #Loop for every tag in the tags list of information to be anonymized.
        for tagHex in tagsHex:
            #Create a tag from the hexadecimal representation of it.
            tag = Tag(tagHex)
            
            #Check if the tag exist in the dicom file
            if tag in dicomFile:
                #If so, remove it.
                dicomFile[tag].value = "REMOVED"
        #Save the modified dicom file.
        dicomFile.save_as(inputDicomPath)
        
        
    @staticmethod
    def cropImage(inputImagePath, outputImagePath, ROIWidth, ROIHeight, ROIXCenter, ROIYCenter):
        """
        cropImage static funciton which crops an image based on given dimesnions and coordinates.
        
        Args:
            inputImagePath  (String): path of the input image, must be PNG.
            outputImagePath (String): path of the output image, must be PNG.
            ROIWidth  (double): width of the ROI.
            ROIHeight (double): height of the ROI.
            ROIXCenter (double): x coordinate of the center of ROI.
            ROIYCenter (double): y coordinate of the center of ROI.
        Result:
            Saves the cropped image.
        
        """
        #Open the file.
        image = Image.open(inputImagePath)

        #Get dimensions of the cropping area.
        leftDimension   = ROIXCenter - ROIWidth/2
        upDimension     = ROIYCenter - ROIHeight/2
        rightDimension  = ROIXCenter + ROIWidth/2
        downDimension   = ROIYCenter + ROIHeight/2

        #Crop the image.
        croppedImage = image.crop((leftDimension,upDimension, rightDimension, downDimension))

        #Save the image.
        croppedImage.save(outputImagePath)
    
    @staticmethod
    def dicomToPNG(inputFilePath, outputFileDir, outputFileName):
        """
        Converts a DICOM file into a PNG file.
        
        Args:
            inputFilePath   (String): path of the input dicom file.
            outputFileDir   (String): directory of the output file (Doesn't inlcude the outputfile).
            outputFileName  (String): Output file name without extension.
        Results:
            Saves the converted PNG image in the output file directory names as output file name.
        
        """
        #Create the DicomConverter object.
        converter = dcmConverter.med2image_dcm(
                                inputFile         = inputFilePath,
                                outputDir         = outputFileDir,
                                outputFileStem    = outputFileName,
                                outputFileType    = "png")
        
        #Run the converter.
        savedImage = converter.run()
        
        savedImage.save(outputFileDir+outputFileName+"Weee.png")
        
        #Open the created png file and convert it to Greyscale.
        image = Image.open(outputFileDir+outputFileName+".png").convert("L")

        #Save the greyscale image.
        image.save(outputFileDir+outputFileName+".png")
