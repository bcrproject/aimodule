"""ExcelHelper module used for operations on Excel files.

This module is used for:
    1) Getting the results of given file names from an excel file.

All functions in this class are static, as they are used as helper functions.
"""

from openpyxl import load_workbook
from Constants import Constants
import numpy



class ExcelHelper:
    """
    ExcelHelper Class used to encapsulate all functions provided by this module.
    """
    
    @staticmethod
    def getImagesResults(excelFilePath, imageNumbersColNumber, resultsColNumber):
        """
        getImagesResults static method, which returns a data structure contianing the results of 
        each image from an excel file. The data structure is as follows: 
            Array[index] = (fileName, fileResult), where:
                - index of the array is the index of the fileName given in the imagesNames array.
                - fileName is the name of the file, given from imagesNames.
                - fileResult, is the the result of the file, from the excel file.
        
        Args:
            excelFilePath  (String): directory of excel file of the results, the excel file has the
            following columns (Column numbers are specified):
                - Column1: IMAGENUMBER
                - Column2: Result as a string.
            imageNumbersColNumber (int): Number of the coloumn that have the imagesNumbers.
            resultsColNumber (int): Number of the column that have the results.
        Returns:
            Data strcuture as specified in the method's description.
        Exceptions: 
            - Not yet implemented.
        """
        
        #Load the excel file as read only.
        wb = load_workbook(filename=excelFilePath, read_only=True)
        ws = wb[Constants.WS_NAME]
        
        
        #Create the array.
        resultsArray = []
        #Loop through all rows in the work sheet.
        for row in ws.rows:
            #Get the image number and the image result.
            imageNumber = row[imageNumbersColNumber].value
            imageResult = (row[resultsColNumber].value)[0:2] #Remove the last number.
            
            #Append to the array.
            resultsArray.append((imageNumber, imageResult))
            
        
        return resultsArray

    @staticmethod
    def saveImagesResults(excelFilePath, imageNumbersColNumber, resultsColNumber, outputArrayPath):
        """
        saveImagesResults static method, which saves a data structure contianing the results of 
        each image from an excel file. The data structure is as follows: 
            Array[index] = (fileName, fileResult), where:
                - index of the array is the index of the fileName given in the imagesNames array.
                - fileName is the name of the file, given from imagesNames.
                - fileResult, is the the result of the file, from the excel file.
        
        Args:
            excelFilePath  (String): directory of excel file of the results, the excel file has the
            following columns (Column numbers are specified):
                - Column1: IMAGENUMBER
                - Column2: Result as a string.
            imageNumbersColNumber (int): Number of the coloumn that have the imagesNumbers.
            resultsColNumber (int): Number of the column that have the results.
            outputFilePath (String): Path of the output array, including the name of the file.
        Returns:
            Data strcuture as specified in the method's description.
        Exceptions: 
            - Not yet implemented.
        """
        
        #Get the images results.
        imagesResults = ExcelHelper.getImagesResults(excelFilePath, imageNumbersColNumber, resultsColNumber)
        
        #Convert it to numpy array.
        imagesArray = numpy.array(imagesResults)
        
        #Save the numpy array.
        numpy.save(outputArrayPath, imagesArray)
        
        
        
            
        
        
        
        
        
        
        
        