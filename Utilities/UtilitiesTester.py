import DicomProcessor as dp
#import IOModels as md
#from JsonManager import JsonManager as js
from ExcelHelper import ExcelHelper as eh
import numpy as np
from Constants import Constants
from sklearn.decomposition import RandomizedPCA
from sklearn.externals import joblib
from PIL import Image
import glob as g
from scipy.spatial import distance
#from OnlineServices import OnlinePCA as op
from OfflineLearning import MLPCA



#results = np.load("./StoredData/ImagesResults.npy")

#twoSets = MLPCA.PCAHelper.splitImagesTwoSets("./TrainingData/TrainingImagesMod/", results, 70, 30)

#result = op.OnlinePCA.analyzeImageUsingPCA("./OnlineServices/1000156_cut_765555.png")


#dp.DicomProcessor.extractROI("./dicomOld.dcm", "./", "THERESULT", 128, 128, 324, 247)
dp.DicomProcessor.createThumbnailFromDicom("./dicomOld.dcm", "./", "thumbnail")



#string = '{"id": 5,"type": "IA","inputDicom": "D:\\\\home\\\\site\\\\wwwroot\\\\images"}'
string = '{"id": 1,"type": "CT","inputDicom": "./Utilities/dicomOld.dcm", "outputName": "DICOMTHUMBNAIL", "outputDir": "./"}'
print "Request: ", js.parseJSONRequest(string)
#eh.saveImagesResults("./Utilities/12er-patches_LG.xlsx", 0, 4, "./StoredData/ImagesResults")
#array = numpy.load("Results.npy")
#MLPCA.MLPCA.performPCA("./OfflineLearning/TrainingImagesMod/","./StoredData/")



"""
means = joblib.load("./StoredData/MeansArrays.pkl")

pca = joblib.load("./StoredData/PCAObject.pkl")
image = Image.open("./OfflineLearning/TrainingImagesMod/996029_cut_797602.png").convert('L')
results = np.load("./StoredData/ImagesResults.npy")
image = np.array(image.getdata())

transformed  = pca.transform(image)
distances = []



transformed = transformed.transpose()

for category in means.keys():    
    distances.append((distance.euclidean(transformed, means[category]), category))

minimum = min(distances, key = lambda t: t[0])

#Get sum of all but the minimum distances
distsSum = sum(dists[0] for dists in distances if dists != minimum)

print "Percentatge:" +  str((distsSum-minimum[0])/distsSum *100) + "%\n"
print "Result:", results[11]
print "Minimum: ", minimum



files = g.glob("./OfflineLearning/TrainingImagesMod/*.png")
files.sort(key=lambda s: int(s[s.rfind('/')+1:].split('_')[0]))

countTrue = 0
countFalse = 0

for i, fileName in enumerate(files):
    image = Image.open(fileName).convert('L')
    image = np.array(image.getdata())
    transformed  = pca.transform(image)

    for category in means.keys():    
        distances.append((distance.euclidean(transformed, means[category]), category))
        
    minimum = min(distances, key = lambda t: t[0])[1]
    
    if minimum[1] == results[i][1][1] or minimum[0] == results[i][1][0]:
        countTrue += 1
    else:
        countFalse += 1
        

print "True: ", countTrue
print "False: ", countFalse
    




array  = np.load("./StoredData/ProjectedComponentsVectors.npy")



csArray = np.load("./StoredData/ColumnStackedImages.npy")

pca = joblib.load("./StoredData/PCAObject.pkl")






#array = array.mean(axis = 1)

#Create the means dictionary
means = dict()


#Loop through all projected components vectors. 
for i, vector in enumerate(array):
    #Get the result of the vector.
    vectorResult = results[i][1]
    
    #If the result has not been seen yet.
    if vectorResult not in means.keys():
        #Create a new mean array for that result.
        means[vectorResult] = np.array(vector)
    #Else if the result has been seen.
    else:
        #Append the vector to the numpy array.
        means[vectorResult] = np.column_stack([means[vectorResult], vector])

        
#Loop through all categorized vectors and compute their means after transposing them.
for category in means.keys():
    #Transpose it.
    means[category] = means[category].transpose()
    #Calculate the mean using the columns.
    means[category] = means[category].mean(axis = 0)
    

print "Means:\n", means['g55']
print "Image:\n", transformed
print "Result:\n", results[6]


print "Transformed:\n", array
print "Image:\n", 
print "Image Results:\n", restuls[6]
"""