"""Constants file which handles all Constnats used in the system.
"""

class Constants:
    #Request keys.
    REQUEST_ID   = "id"
    REQUEST_TYPE = "type"
    REQUEST_INPUT_DICOM = "inputDicom"
    REQUEST_OUTPUT_DIR = "outputDir"
    REQUEST_OUTPUT_NAME = "outputName"
    REQUEST_ROI_X = "roiX"
    REQUEST_ROI_Y = "roiY"
    REQUEST_ROI_WIDTH = "roiWidth"
    REQUEST_ROI_HEIGHT = "roiHeight"
    
    #Response Keys.
    RESPONSE_ID = "id"
    RESPONSE_CLASS = "Classification"
    RESPONSE_DIAGNOSIS = "Diagnosis"
    
    #Request Types.
    CT_REQUEST = "CT"
    SR_REQUEST = "SR"
    SM_REQUEST = "SM"
    
    #Files.
    #Excel
    WS_NAME        = "12er-patches"
    EXCEL_RESULTS  = "ImagesResults.npy"
    #MLPCA
    PCA_PCVNAME    = "ProjectedComponentsVectors"
    PCA_CSINAME    = "ColumnStackedImages"
    PCA_OBJNAME    = "PCAObject.pkl"
    PCA_MNSNAME    = "MeansArrays.pkl"
    #MLSVM
    SVM_OBJNAME    = "SVMObject.pkl"
    #OnlinePCA
    DATA_PATH      = "./StoredData/"#"c:/users/abdulah/documents/visual studio 2013/Projects/BCRProject/BCRProject/AIOnlineModule/AIModule/StoredData/"
    MEANS_NAME     = "MeansArrays.pkl"
    PCA_NAME       = "PCAObject.pkl"
    #INPUT_DATA
    TRNG_IMGS      = "TrainingImagesArray.npy"
    TSTNG_IMGS     = "TestingImagesArray.npy"
    TRNG_RSLTS     = "TrainingImagesResults.npy"
    TSTNG_RSLTS    = "TestingImagesResults.npy"
    
    
    #Constant numbers.
    COMPRESS_QUALITY = 50
    
    #Printing.
    CURSOR_UP_ONE = '\x1b[1A'
    ERASE_LINE    = '\x1b[2K'
    PRINT_ERASE   = CURSOR_UP_ONE + CURSOR_UP_ONE + ERASE_LINE + CURSOR_UP_ONE