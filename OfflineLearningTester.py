from OfflineLearning.MLPCA import PCAHelper as ph 
from OfflineLearning.LGTrainingDataManager import LGTrainingDataManager
import numpy as np
from PIL import Image
from Utilities.ExcelHelper import ExcelHelper as eh
from Utilities.Constants import Constants
from OfflineLearning.MLPCA import MLPCA as mpca
from OfflineLearning.MLSVM import MLSVM as msvm
from Testing.AITester import AITester as tester


#mpca.performPCA("./StoredData/", True, True, nComponents = 45)
#msvm.performSVM("./StoredData/")


#tester.testImageAnalysis()




"""
dmObject = LGTrainingDataManager()

dmObject.saveTrainingAndTestingImagesWithResults("./TrainingData/TrainingImagesMod/", 70) 


trainingSet    = np.load("./TrainingData/TrainingImagesArray.npy")
testingSet     = np.load("./TrainingData/TestingImagesArray.npy")
trainigResults = np.load("./TrainingData/TrainingImagesResults.npy")
testingResults      = np.load("./TrainingData/TestingImagesResults.npy")




image = Image.open("./TrainingData/TrainingImagesMod/1001068_cut_797922.png").convert('L')
image = np.array(image.getdata())

i =0 
for trainingImage in trainingSet:
    if (trainingImage == image).all():
        print "TrainingImage:\n", trainingImage
        print "image:\n", image
        print "Result:", trainigResults[i]
    i += 1
    
i =0 
for testingImage in testingSet:
    if (testingImage == image).all():
        print "TestingImage:\n", testingImage
        print "image:\n", image
        print "Result:", testingResults[i]
    i += 1
        
"""     
        