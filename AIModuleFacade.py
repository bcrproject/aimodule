"""AIModuleFacade is the facade module for the whole AIModule. It's used by the server to request     services from the AIModule. The interactions are done using JSON strings as messages. 

It's not encapsulated in a class, to allow direct execution by the server.
"""


#Import statements.
from Utilities import *
from InterfaceManagement.JsonManager import JsonManager  #JsonManager Class.
from Utilities.DicomProcessor import DicomProcessor
import sys       #To allow arguments to be parsed.





def startExecution():
    """
    Method to start the execution of the Facade module.
    """
    #Check if the server provided input. 
    if checkArgs(sys.argv):
        #Parse the JSON Request.
        request = JsonManager.parseJsonRequest(sys.argv[1])
        
        print "", request.performRequest()



def checkArgs(argsList):
        """
        Method that checks if the provided arguments list contains a string request.
        Arguments:
            argsList (List): list of arguments provided.
        Retruns:
            True, if the supplied list has a string argument, False otherwise.
        """
        #Check if there's an argument.
        if len(argsList) > 1:
            #Check if the provided argument is a string.
            if isinstance(argsList[1], str): 
                return True #Return true. 
        return False #If otherwise
        
        
    

#Start the module execution.
startExecution()
    