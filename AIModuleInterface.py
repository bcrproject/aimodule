"""AIModuleInterface Used to provide an interface to the AIModule to use its services interactively as an interpreter type application.

This script is not encapsulated in class, so it can be used immediately when executed. 
"""

#import System standard input for reading from console.
from sys import stdin

#import AIModule packages.
from OfflineLearning.MLPCA import MLPCA as mpca
from OfflineLearning.MLSVM import MLSVM as msvm
from Testing.AITester import AITester
from Utilities.IRMANamingHelper import IRMANamingHelper as namer
from OnlineServices.OnlineServicesFacade import OnlineServicesFacade as osf
import sys

#import ArgsParser library for parsing Arguments.
import argparse

#Modify the logger.

def createParser():
    """
    Function that creates a parser object.
    """
    
    #Create parser object.
    parser = argparse.ArgumentParser(prog="Amal Artificial Intelligence Module",
                                      description="This module enables you to access Amal AI services interactively!\nYou can analyze mammograms, request report of system's accuracy, and train the system.",
                                    epilog="You can use more than one option, all will be executed.")
    
    
    #Create group of arguments.
    group = parser.add_argument_group('Commands', 'Available commands to execute')
    
    #Add argument for analyzing a mammogram.
    group.add_argument('-am','--AnalyzeMammogram', nargs = 1, 
                        dest = 'imagePath', help = "Analyze a given mammogram, returns its predicted diagnosis and fat density category. Must supply the relative path of the image (including its full name). The image must be a region of interest PNG image (128*128).")
    
    
    #Add argument for Training the system.
    group.add_argument('-train','--TrainSystem', nargs = 1, 
                        dest = 'outputPath', help = "Train the system on a the available set of data, output the results into the given relative path.")
    
    #Add argument for testing.
    group.add_argument('-test','--testSystem', action = 'store_true', 
                        help = "Test the accuracy of the sytem, the option will generate a confusion matrix, and a report for the percentage of accurate predictions.")
    
    
    return parser


def execute(commands):
    """
    Function that takes a namespace object of commands, and execute the ones that are available.
    """
    
    #Test Command.
    if commands.testSystem == True:
        print "Performing Testing..."
        AITester.testImageAnalysis()
        
    #Analyze Command.
    if commands.imagePath != None:
        print "Performing Analysis..."
        result = osf.analyzeImagePath(commands.imagePath[0])
        print "Result of analysis: ", namer.getResultString(result)
        
    
    #Train Command.
    if commands.outputPath != None:
        print "Performing Training..."
        mpca.performPCA("./Test/", True, True, nComponents = 45)
        msvm.performSVM("./Test/")
        
        

def main():
    #Create the args parser object.
    parser = createParser()
    
    #Print the Usage.
    print parser.print_help()

    #infinite loop for reading the input.
    while(True):
        #Prompt to enter input:
        #Using sys.write to print without new line
        sys.stdout.write("Enter your command > ")
        
        #Take the input
        line = stdin.readline().rstrip('\n')

        try:
            #Extract the commands.
            commands = parser.parse_args(line.split())
        except (Exception): 
            print "Error"
        
        #Execute the commands.
        execute(commands)
        
        
        
#Call the main function, to execute the script.
main()
    


    








