"""
CTRequest model class is a subclass of Request.
Create Thumbnail service request
""" 

from IOModels import Request 
from Utilities.DicomProcessor import DicomProcessor as dp
from Utilities.Constants import Constants as consts
import json


#Default parameters of the ct request.
ctParams = [
    consts.REQUEST_ID,
    consts.REQUEST_TYPE,
    consts.REQUEST_INPUT_DICOM,
    consts.REQUEST_OUTPUT_DIR,
    consts.REQUEST_OUTPUT_NAME
]


class CTRequest(Request):
    
    def __init__(self, params):
        """
        Initializer that takes 1 argument.
        Args:
            params (dictionary): parameters of the ct request.
        Raises:
            TypeError, if one of the entered values are incorrect.
        """
        #Check the parameters for correctness.
        if not self.checkParameters(params):
            raise TypeError("One or more of the input types is/are not correct.")

        #Else 
        else:
            #Initilize superclass.
            super(CTRequest, self).__init__(params)
            
            #Initilize the attributes.
            self.inputDicom = params[consts.REQUEST_INPUT_DICOM]
            self.outputDir  = params[consts.REQUEST_OUTPUT_DIR]
            self.outputName = params[consts.REQUEST_OUTPUT_NAME]
     
        
    def checkParameters(self, params):
        """
        Method that checks all the parameters of the request function.
        Args:
            Same as the initilizer.
        Return:
            True, if the types are correct.
            False, otherwise.
        """
        return set(params.keys()) == set(ctParams) 
            
        
        
    def performRequest(self):
        """
        Method that performs a CTRequest.
        Args:
            Same as the initilizer.
        Return:
            response json.
        """
        #Create thumbnail using dicom processor.
        dp.createThumbnailFromDicom(self.inputDicom, self.outputDir, self.outputName)
        
        #Create the response.
        response = {consts.RESPONSE_ID: self.id}
        
        
        #return the response.
        return json.dumps(response)
        
        
        
        
        
        
