"""JsonManager handles parsing of JSON requests and serialization of responses.

This module is used for:
    1) Parse JSON request and return it as an object.
    2) Serialize a response and convert it to JSON.
All methods in this class are static, as they are used as helper methods.
"""

import json
import IOModels as models
from Utilities.Constants import Constants
from CTRequest import CTRequest
from SRRequest import SRRequest
from SMRequest import SMRequest

class JsonManager:
    """
    Main Class used to encapsulate all functions provided by this module.
    """
    
    @staticmethod
    def parseJsonRequest(jsonRequestString):
        """
        Parses a request as a JSON string, and returns a request object.
        
        Args:
            jsonRequestString (String): JSON string represnting the request.
        Returns:
            Request Object.
        """
        #Parse the json string into a dictionary. 
        jsonRequest = json.loads(jsonRequestString)
        
        #Check the type.
        if (Constants.REQUEST_TYPE in jsonRequest.keys()):
            #Get the request type
            rType = jsonRequest[Constants.REQUEST_TYPE]

            #Create the appropriate request type.
            try:
                if rType == Constants.CT_REQUEST:
                    return CTRequest(jsonRequest)
                elif rType == Constants.SR_REQUEST:
                    return SRRequest(jsonRequest)
                elif rType == Constants.SM_REQUEST:
                    return SMRequest(jsonRequest)
                
            except (ValueError, TypeError):
                print "Error"

                

            