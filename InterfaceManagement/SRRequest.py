"""
SRRequest model class is a subclass of Request.
Submit ROI Service Request.
""" 

from IOModels import Request 
from Utilities.DicomProcessor import DicomProcessor as dp
from Utilities.Constants import Constants as consts
from OnlineServices.OnlineServicesFacade import OnlineServicesFacade as op
from Utilities.IRMANamingHelper import IRMANamingHelper as ir
import json

#Default parameters of the SR request.
srParams = [
    consts.REQUEST_ID,
    consts.REQUEST_TYPE,
    consts.REQUEST_INPUT_DICOM,
    consts.REQUEST_OUTPUT_DIR,
    consts.REQUEST_OUTPUT_NAME,
    consts.REQUEST_ROI_X,
    consts.REQUEST_ROI_Y,
    consts.REQUEST_ROI_WIDTH,
    consts.REQUEST_ROI_HEIGHT,
]


class SRRequest(Request):
    
    def __init__(self, params):
        """
        Initializer that takes 1 argument.
        Args:
            params (dictionary): parameters of the ct request.
        Raises:
            TypeError, if one of the entered values are incorrect.
        """
        #Check the parameters for correctness.
        if not self.checkParameters(params):
            raise TypeError("One or more of the input types is/are not correct.")

        #Else 
        else:
            #Initilize superclass.
            super(SRRequest, self).__init__(params)
            
            #Initilize the attributes.
            self.inputDicom = params[consts.REQUEST_INPUT_DICOM]
            self.outputDir  = params[consts.REQUEST_OUTPUT_DIR]
            self.outputName = params[consts.REQUEST_OUTPUT_NAME]
            self.roiX       = params[consts.REQUEST_ROI_X]  
            self.roiY       = params[consts.REQUEST_ROI_Y]  
            self.roiWidth   = params[consts.REQUEST_ROI_WIDTH]  
            self.roiHeight  = params[consts.REQUEST_ROI_HEIGHT]  
     
        
    def checkParameters(self, params):
        """
        Method that checks all the parameters of the request function.
        Args:
            Same as the initilizer.
        Return:
            True, if the types are correct.
            False, otherwise.
        """
        return set(params.keys()) == set(srParams) 
            
        
        
    def performRequest(self):
        """
        Method that performs a SRRequest.
        Args:
            Same as the initilizer.
        Return:
            response json.
        """
        
        #First, extract the ROI.
        dp.extractROI(self.inputDicom, self.outputDir, self.outputName, self.roiWidth, self.roiHeight, self.roiX, self.roiY)
        
        
        #Second, perform analysis on ROI, using online services.
        result = op.analyzeImagePath(self.outputDir+self.outputName+".png")    
        
        classif, diag = ir.convertIRMAResult(str(result))
        
        response = {consts.RESPONSE_ID: self.id,
                    consts.RESPONSE_CLASS: classif,
                    consts.RESPONSE_DIAGNOSIS: diag
                   }
        
        return json.dumps(response)
        
        
        
        
        
        
