"""IOModels contain model classes and enumerations used for I/O interactions (Requests/Responses).

This module contains:
    1) Request class.
    2) RequestTypes Enum.
    3) RequestInputs Enum.
""" 

from flufl.enum import Enum
from Utilities.Constants import Constants
import json

#Request parameters.
requestParams = [
    Constants.REQUEST_ID,
    Constants.REQUEST_TYPE
]

class Request(object):
    """
    Model class used to encapsulate the request object.
    Each request object contain the following attributes:
        1) params: dictionary of parameters.
    """
    
    def __init__(self, params):
        """
        Initializer that takes 1 argument.
        
        Args:
            params (Dictionary): Parameters of the request, all parameters must be valid.
        Raises:
            TypeError, if one of the entered values are incorrect.
        """
        
        
        #Check the Parameters.
        if not self.checkParameters(params):
            raise TypeError("One or more of the input types is/are not correct.")
        
        else:
            #initilize the variables.
            self.id   = params[Constants.REQUEST_ID]
            self.type = params[Constants.REQUEST_TYPE]
        
        
    def checkParameters(self, params):
        return set(params.keys()) == set(requestParams)

    
    def __str__(self):
        """
        String method to return a string representation of the request.
        """
        string = "ID = {0}, Type: {1}".format(self.id, self.type)
        return string

    
class Response(object):
    """
    Model class used to encapsulate the response object.
    Each request object contain the following attributes:
        1) id: int.
    """
    
    def __init__(self, responseId):
        self.responseId = responseId
        
    def __str__(self):
        """
        String method to return a string representation of the response.
        """
        return str(self.responseId)
        
    def toJSON(self):
        """
        Method that convers the response to json. 
        """
        
        #Add the response id to the attributes of the response.
        self.attributes["id"] = self.responseId
        
        #convert it to json and return it.
        return json.dumps(self.attributes)
    
class RequestTypes(Enum):
    """
    Enumeration class, which contains all available requestTypes.
    """
    IA = 1
    CT = 2
    
    
class RequestInputs(Enum):
    """
    Enumeration class, which contains all available inputs types for requests.
    Used as Keys for the requests' inputs dictionaries.
    """
    inputDicom = Constants.REQUEST_INPUT_DICOM
    outputDir  = Constants.REQUEST_OUTPUT_DIR
    outputName = Constants.REQUEST_OUTPUT_NAME