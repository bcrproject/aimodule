"""AITester used to test the accuracy of the prediction of the Artificial intelligence analysis.
It uses the testing data prepared by the TrainingDataManagers. 
"""
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import numpy as np
from Utilities.Constants import Constants
from OnlineServices.OnlineServicesFacade import OnlineServicesFacade as osf

class AITester:
    """
    Main class, encapsulates all testing functionalities as static methods. 
    """
    
    @staticmethod
    def testImageAnalysis(plotResults = True):
        """
        method to test the analysis of images using OnlineServices, it shows the accuracy of the AI 
        Prediction.
        
        Args:
            - plotResults (boolean): True by default, indiciates wether to plot the results.
        """
        
        #Get the testing images. 
        testingImages = np.load("./TrainingData/" + Constants.TSTNG_IMGS)
        
        #Get the testing images results.
        actualResults = np.load("./TrainingData/" + Constants.TSTNG_RSLTS)
        
        #List of results.
        results = []
        
        #Get the predicted results of the images. 
        for image in testingImages:
            #Use the online services for the analysis.
            results.append(osf.analyzeImage(image))
        
        #Labels to show in the confusion matrix.
        labels = ["f1", "f2", "d1", "d2", "d5", "e1", "e2", "e5", "g1", "g2", "g5"]

        #Construct the confusion matrix.
        matrix = confusion_matrix(actualResults, results, labels = labels)
        
        #Plot the confusion matrix:
        if plotResults:
            #For ploting.
            plt.figure()
            #Plot the confusion matrix
            AITester.plot_confusion_matrix(matrix, labels)

        
        
        #Print the confusion matrix.
        print "Confusion Matrix:\n", matrix
        
        #Print the count of false and 
        countTrue = 0
        countFalse = 0
        for i, image in enumerate(testingImages):
            #Print the results
            #print "Image " + str(i) + ": " + actualResults[i] + " - " + results[i]
            if actualResults[i] == results[i]:
                countTrue += 1
            else:
                countFalse += 1
                
        truePercent = float(countTrue)/(countTrue+countFalse)*100
        falsePercent = float(countFalse)/(countTrue+countFalse)*100
        #Print the results.
        print "\nPercentage of correct predictions: {0:.2f}%".format(truePercent)
        print "Percentage of false   predictions: {0:.2f}%".format(falsePercent)
        
        if plotResults:
            #Show the plot.
            plt.show()
    
    @staticmethod
    def plot_confusion_matrix(cm, labels, title='Confusion matrix', cmap=plt.cm.Blues):
        """
        Method that plots the confusion matrix, it uses Matplotlib.
        """
        plt.imshow(cm, interpolation='nearest', cmap=cmap)
        plt.title(title)
        plt.colorbar()
        tick_marks = np.arange(len(labels))
        plt.xticks(tick_marks, labels, rotation=45)
        plt.yticks(tick_marks, labels)
        plt.tight_layout()
        plt.ylabel('True label')
        plt.xlabel('Predicted label')
        
        
        
        
        
        
        
        
        
        
        