"""OnlineServicesFacade used as an interface to the OnlineServicesModule. The interface provides all analysis services, while hiding the complexity of algorithms. The user will get a diagnosis without specifying which algorithm to use. 
"""

from OnlinePCA import OnlinePCA as op
from OnlineSVM import OnlineSVM as svm
from PIL import Image

class OnlineServicesFacade:
    """
    Main class to encapsulate all static methods.
    """
    
    @staticmethod
    def analyzeImagePath(imagePath):
        """
        Analyzes an image by comparison of means projected components vectors using euclidean   
        distances.
        
        Args:
            - imagePath (String): full path of the ROI image to be analyzed.
        Returns:
            - result (String): Result of the image.
        """
        
        #Return the image analysis using PCA.
        return svm.analyzeImagePathUsingSVM(imagePath)
    
    @staticmethod
    def analyzeImage(image):
        """
        Analyzes an image by comparison of means projected components vectors using euclidean   
        distances, uses a pillow image instead of image path.
        Args:
            - image (Image): image to be analyzed
        Returns:
            - result (String): Result of the image.
        """
        
        #return the image analysis
        return svm.analyzeImageUsingSVM(image)
