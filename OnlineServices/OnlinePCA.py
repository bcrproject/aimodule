"""OnlinePCA module used to perform Principal Component Analysis on a testing image, compare it
with Projected Components of training images, and return its result.

This module is used to:
    1) Perform PCA on a testing Image.
    2) Compare the image to the training images using euclidean distance.
    3) Return a result of an image.
All methods in this class are static, so they can be used directly without creating an object.
"""


from sklearn.decomposition import RandomizedPCA  #Used to project the testing image.
from sklearn.externals import joblib             #Used to load the pca object & means arrays.
from scipy.spatial import  distance as edistance #Used to calculate the euclidean distance.
from Utilities.Constants import Constants        #Constants of files and paths.
from PIL import Image                            #Image library.
import numpy as np                               #Numpy arrays.


class OnlinePCA:
    """
    OnlinePCA Class encapsulates all functionalities to perform online image analysis using PCA. 
    """
    
    @staticmethod
    def analyzeImageUsingPCA(image):
        """
        Analyzes an image by comparison of means projected components vectors using euclidean   
        distances.

        
        Args:
            - image (Image): image to be analyzed
        Returns:
            - result (String): Result of the image.
        """
        #Get the PCA object. 
        pca = joblib.load(Constants.DATA_PATH + Constants.PCA_NAME)
        
        
        #Get the means arrays
        means = joblib.load(Constants.DATA_PATH + Constants.MEANS_NAME)
        
        #Convert it to numpy array.
        image = np.array(image)
        
        #Compute and return the result
        return OnlinePCAHelper.getImageResultByEuclideanDistance(means, pca, image)
    
    @staticmethod
    def analyzeImagePathUsingPCA(imagePath):
        """
        Overloading of the method, uses the path of the image instead of the actual image.
        
        Args:
            - imagePath (String): full path of the ROI image to be analyzed.
        Returns:
            - result (String): Result of the image.
        """
        #Get the image data, and ensure it's a grayscale one.
        image = Image.open(imagePath).convert("L").getdata()
        
        #Compute and return the result
        return analyzeImageUsingPCA(image)
    

    
    
class OnlinePCAHelper:
    """
    OnlinePCAHelper class encapsulates all helper methods useful for performing image analysis. 
    """
    
    @staticmethod
    def getImageResultByEuclideanDistance(means, pca, image):
        """
        Returns the result of an image, which is the one corresponding to the result of the category
        which has the least euclidean distance from their mean to the projected image vector.
        
        Args:
            - means (dictionary): Dictionary of means of categories.
            - pca (RandomizedPCA object): PCA object fit with training images.
            - image (PIL.Image object): Testing image.
        Returns:
            - result (String): Result of the image.
        """  
        
        #Project the image.
        imageVector = pca.transform(image) 
        
        #Create a distances list.
        distances = []
        
        #Load through all categories of the means.
        for category in means.keys():   
            
            #Calculate the distance from the mean to the image vector.
            distance = edistance.euclidean(imageVector, means[category])
            
            #Append it as a tuple with the category to the distances list.
            distances.append((distance, category))
            
        #Get the category of the image, given the least distance.
        imageCategory = min(distances, key = lambda t: t[0])[1]
        
        return imageCategory
            
            
            
            
            
            
            
            
            
        
        
        
        
        
        