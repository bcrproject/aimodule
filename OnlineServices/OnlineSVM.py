"""OnlineSVM module used to perform support vector machines classficiation algorithm on a testing image, using the projected components vectors acquired from MLPCA.

All methods in this class are static, so they can be used directly without creating an object.
"""


from sklearn.decomposition import RandomizedPCA  #Used to project the testing image.
from sklearn.externals import joblib             #Used to load the pca object & means arrays.
from Utilities.Constants import Constants        #Constants of files and paths.
from PIL import Image                            #Image library.
import numpy as np                               #Numpy arrays.
from sklearn.svm import SVC


class OnlineSVM:
    """
    OnlineSVM Class encapsulates all functionalities to perform online image analysis using SVM. 
    """
    
    @staticmethod
    def analyzeImageUsingSVM(image):
        """
        Analyzes an image using SVM classifiers fit with projected component vectors.
        
        Args:
            - image (Image): image to be analyzed
        Returns:
            - result (String): Result of the image.
        """
        #Get the pca
        pca  = joblib.load(Constants.DATA_PATH + Constants.PCA_NAME)
        
        #Project the image.
        projectedImage = pca.transform(image)

        #load.
        classifier = joblib.load(Constants.DATA_PATH + Constants.SVM_OBJNAME)

        #return the prediction.
        return classifier.predict(projectedImage)[0]
    
    
    @staticmethod
    def analyzeImagePathUsingSVM(imagePath):
        """
        Overloading of the method, uses the path of the image instead of the actual image.
        
        Args:
            - imagePath (String): full path of the ROI image to be analyzed.
        Returns:
            - result (String): Result of the image.
        """
        #Get the image data, and ensure it's a grayscale one.
        image = Image.open(imagePath).convert("L").getdata()
        
        #Compute and return the result
        return OnlineSVM.analyzeImageUsingSVM(image)
        

        
        
        
        